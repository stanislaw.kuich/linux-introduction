# Linux Introduction - materiały dodatkowe


Warte posłuchania:

https://www.redhat.com/en/command-line-heroes/season-1/os-wars-part-2-rise-of-linux

Ciekawe linki:

https://www.redhat.com/en/blog/25-years-linux

Przyklad skryptu w Bashu

```
#!/bin/bash
# Usage: bash skrypt_demo users.txt
# Script to create user accounts and assigned them to group which is defined in var1 variable
# users.txt contains name of users in new line
# Example: users.txt
# test1
# test2
# test3
var1=team_2023

sudo groupadd "$var1"
echo "Created group $var1"
for n in `cat $1`
do
 sudo useradd -m "$n"
 echo "Created $n account"
 sudo usermod -a -G "$var1" "$n"
 echo "Added $n account to $var1 group" 
done

```

Przydatne polecenia

```
sed 's/\/bin\/bash/\/bin\/sh/g' /etc/passwd > copy_passwd_nowy

echo "szkolenie linux" | awk '{ print $1}'

find /etc -type f -name "*.conf" -exec du -h {} \;

ip address show | grep -oP "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"

for n in {A,MX,PTR,TXT}; do dig @8.8.8.8 google.com $n +answer; done

ls -lRa /etc/ &> /tmp/test_all

ls -lRa /etc/ 1> /tmp/test_only_good_results

ls -lRa /etc/ 2> /tmp/test_error

cat /etc/shadow 2> errors3.txt || echo "TAK"

cat /etc/shadow 2> errors2.txt

wc -l < /tmp/test_error

for i in test test1 test2 test4 test5 ; do echo "test123" > $i; done

```